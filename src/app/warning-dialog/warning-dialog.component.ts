import { Component, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-warning-dialog',
  templateUrl: './warning-dialog.component.html',
  styleUrls: ['./warning-dialog.component.css'],
  standalone: true,
  imports:[MatDialogModule]
})
export class WarningDialogComponent {

  constructor (private dialogRef: MatDialogRef<WarningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data){
      this.description = data.description;
      this.title = data.title;
    }

    description:string;
    title:string;

    close() {
      this.dialogRef.close();
  }
    


}
