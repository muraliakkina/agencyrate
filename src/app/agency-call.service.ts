import { Injectable,Signal,signal } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AddAgencyRequest, AgencyDto, AgencyInitialRequest, AgencyInitialResponse, Make, MakeResponse, Model, ModelResponse, Spec, SpecResponse } from './Model/agencyRateModels';
import { map,catchError, Observable, throwError } from 'rxjs';
import { toSignal, toObservable } from '@angular/core/rxjs-interop';

@Injectable({
  providedIn: 'root'
})
export class AgencyCallService {

  constructor(private http: HttpClient) { }

  private url = 'http://localhost:';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })};

  private makes$ = this.http.get<MakeResponse>(this.url).pipe(
    map(res => res.result.map(res => ({...res}) as Make))
  )

  private models$ = this.http.get<ModelResponse>(this.url).pipe(
    map(res => res.result.map(res => ({...res }) as Model))
  )

  private specs$ = this.http.get<SpecResponse>(this.url).pipe(
    map(res => res.result.map(res => ({...res}) as Spec))
  )

   getAgencyRates(initialRequest: AgencyInitialRequest):Observable<AgencyDto[]>{
     return  this.http.post<AgencyInitialResponse>(this.url,initialRequest).pipe(
      map(response => response.vehicleData.map(vehicle => ({...vehicle}) as AgencyDto)
    ),
    catchError(() => {throw new Error(`${this.url}`)}));
  }

  addAgencyRates(body: AddAgencyRequest):Observable<any[]>{
    return this.http.post<any>(this.url,body).pipe((res)=> res);
  }

 

  // makes = toSignal(this.makes$,{initialValue:[] as Make[]})
  // models = toSignal(this.models$,{initialValue:[] as Model[]})
  // specs = toSignal(this.specs$,{initialValue:[] as Spec[]})



}
