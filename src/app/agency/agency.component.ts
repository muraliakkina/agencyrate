import { Component, OnInit, ViewChild, signal } from '@angular/core';
import { AgencyDto, AgencyInitialRequest } from '../Model/agencyRateModels';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AgencyCallService } from '../agency-call.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { WarningDialogComponent } from '../warning-dialog/warning-dialog.component';


@Component({
  selector: 'app-agency',
  templateUrl: './agency.component.html',
  styleUrls: ['./agency.component.css']
})
export class AgencyComponent implements OnInit {

  // agencyDataArr = signal<AgencyDto[]>([]);
  // agencyDataArr:AgencyDto[] = [];
  agencyInitialRequest: AgencyInitialRequest = {};
  isEditable: boolean = false;
  isButtonEnabled: boolean = false;
  makeList = signal(['BMW', 'AUDI', 'Ferrari', 'Mercedes']);
  // makeList = this.agencyCallService.makes
  makeSelected: string;
  modelSelected: string;
  modelList = signal(['AMG', 'R8', 'M230']);
  // modelList = this.agencyCallService.models;
  specSelected: string;
  specList = signal(['spec1', 'spec2', 'spec3', 'spec4', 'spec5', 'spec6']);
  // specList = this.agencyCallService.specs;
  yearSelected: number;
  yearList: number[] = [2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023];

  isLoadingResults: boolean = false;
  isRateLimitReached: boolean = false;
  agencyForms: FormGroup;
  dataSource = new MatTableDataSource<any>();
  horizontalPosition: MatSnackBarHorizontalPosition;
  verticalPosition: MatSnackBarVerticalPosition;

  agencyDataArr = signal<AgencyDto[]>([
    {
      package: "SS",
      make: "Test1",
      model: "Test1Model",
      spec: "Test1Species",
      year: 2014,
      agencyYear: 1,
      agencyRate: 2.38,
      repairCondition: 1
    },
    {
      package: "SS",
      make: "Test1",
      model: "Test1Model",
      spec: "Test1Species",
      year: 2014,
      agencyYear: 2,
      agencyRate: 2.7,
      repairCondition: 1
    },
    {
      package: "SS",
      make: "Test1",
      model: "Test1Model",
      spec: "Test1Species",
      year: 2014,
      agencyYear: 3,
      agencyRate: 3,
      repairCondition: 1
    },
    {
      package: "SS",
      make: "Test1",
      model: "Test1Model",
      spec: "Test1Species",
      year: 2014,
      agencyYear: 0,
      agencyRate: 2.1,
      repairCondition: 2
    }
  ]);

  // agencyDataArr = this.agencyCallService.getAgencyRates(this.agency

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.  
    this.agencyForms = this._formBuilder.group({
      angencyRows: this._formBuilder.array([])
    })

    this.agencyForms = this.fb.group({
      agencyRows: this.fb.array(this.agencyDataArr().map(val => this.fb.group({
        package: new FormControl(val.package),
        make: new FormControl(val.make,[Validators.required]),
        model: new FormControl(val.model,[Validators.required]),
        spec: new FormControl(val.spec,[Validators.required]),
        year: new FormControl(val.year,[Validators.required]),
        agencyYear: new FormControl(val.agencyYear,[Validators.required]),
        agencyRate: new FormControl(val.agencyRate,[Validators.required]),
        repairCondition: new FormControl(val.repairCondition,[Validators.required])
      })))
    })

    this.dataSource = new MatTableDataSource((this.agencyForms.get('agencyRows') as FormArray).controls)

  }

  constructor(private _liveAnnouncer: LiveAnnouncer, private fb: FormBuilder, private _formBuilder: FormBuilder, private _snackBar: MatSnackBar, private agencyCallService: AgencyCallService, private dialog: MatDialog) { }

  displayedColumns: string[] = ['package', 'make', 'model', 'spec', 'year', 'agencyYear', 'agencyRate', 'repairCondition'];
  // dataSource = new MatTableDataSource<AgencyDto>(this.agencyDataArr());

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // console.log(this.dataSource);
  }


  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  toogleIsEditable() {
    this.isEditable = !this.isEditable;
    this.isButtonEnabled = !this.isButtonEnabled;
    console.log('from edit ' + this.isEditable);
    this._snackBar.open('You can edit the details !!!', 'Close', {
      horizontalPosition: "center",
      verticalPosition: "bottom",
    });
    console.log(this.agencyDataArr());
    console.log(this.agencyForms);
    console.log(this.agencyForms.value.agencyRows);
  }

  toogleIsSaveable() {
    this.isEditable = !this.isEditable;
    this.isButtonEnabled = !this.isButtonEnabled;
    console.log('from save ' + this.isEditable);
    this._snackBar.open('deatils are saved. Please click on Submit !!!', 'Close', {
      horizontalPosition: "center",
      verticalPosition: "bottom",
    });
    console.log(this.agencyForms.pristine);
    console.log(this.agencyForms.value.agencyRows);
  }

  toogleIsCancelable() {
    this.isEditable = !this.isEditable;
    this.isButtonEnabled = !this.isButtonEnabled;
    console.log('from cancel ' + this.isEditable);
    this._snackBar.open('data is not saved. please click on edit to modify the data !!!', 'Close', {
      horizontalPosition: "center",
      verticalPosition: "bottom",
    });
    this.agencyForms.get('make').reset((val =>{
      val = this.agencyDataArr()[0]
    }))
  }

  searchRates() {
    if (this.makeSelected === undefined) {
      this.openDialog({title:'Warning !!',description:'First Please select the Make. If Make is not available please select the any Make and then edit the below data to add the new Make'})
      // window.alert('First Please select the Make. If Make is not available please select the any Make and then edit the below data to add the new Make');
      return
    }
    if (this.modelSelected === undefined) {
      this.openDialog({title:'Warning !!',description:'First Please select the Model. If Model is not available please select the any other Model and then edit the below data to add the new Model'})
      // window.alert();
      return
    }
    this.agencyInitialRequest.make = this.makeSelected === undefined ? null : this.makeSelected;
    this.agencyInitialRequest.model = this.modelSelected === undefined ? null : this.modelSelected;
    this.agencyInitialRequest.spec = this.specSelected === undefined ? null : this.specSelected;
    this.agencyInitialRequest.year = this.yearSelected === undefined ? null : this.yearSelected;
    // console.log(this.makeSelected + '-' + this.modelSelected + '-'+ this.specSelected +'-'+ this.yearSelected);
    console.log(this.agencyInitialRequest);
    // this.agencyCallService.getAgencyRates(this.agencyInitialRequest).subscribe(data => this.agencyDataArr.set(data));
  }

  openDialog(content: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = content;

    this.dialog.open(WarningDialogComponent, dialogConfig);
  }

  showFiledValue(){

  }

}






