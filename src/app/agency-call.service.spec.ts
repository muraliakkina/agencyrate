import { TestBed } from '@angular/core/testing';

import { AgencyCallService } from './agency-call.service';

describe('AgencyCallService', () => {
  let service: AgencyCallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgencyCallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
