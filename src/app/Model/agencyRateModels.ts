export interface AgencyDto {
    make:string;
    model:string ;
    spec:string ;
    year:number;
    agencyYear:number;
    agencyRate:number;
    repairCondition:number;
}

export interface Make {
    make: string;
}

export interface Model {
    model: string;
}

export interface Spec{
    spec:string;
}

export interface AgencyInitialRequest {
    make?: string;
    model?: string;
    spec?: string;
    year?: number;
}

export interface AgencyInitialResponse {
    AGENCY_RATES: AgencyDto[];
}

export interface AddAgencyRequest{
    vehicleData: AgencyDto[];
}

export interface MakeResponse{
    MAKE_LIST: Make[];
}

export interface ModelResponse{
    MODEL_LIST: Model[];
}

export interface SpecResponse{
    SPEC_LIST: Spec[];
}

export interface AddAgencyResponse{
    AGENCY_RATES: object
}

export interface baseResponse{
    txn_id: number;
    response:object;
    isError: boolean;
    errorCode: number;
    errorMessage: string;
}